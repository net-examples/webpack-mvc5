# (NET FRAMEWORK Or NET CORE) + WEBPACK + REACT JS :blue_heart:

Build modern web applications.

### Required

* [NODE JS](https://nodejs.org/es/) Evented I/O for backend
* [NPM](https://www.npmjs.com/) Package manager
* [WEBPACK](https://webpack.github.io/) Module bundler
* [WEBPACK TASK RUNNER FOR VISUAL STUDIO(OPTIONAL)](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.WebPackTaskRunner)

### package.json

```sh
{
  "devDependencies": {
    "webpack": "^2.4.1",
    "babel-core": "^6.24.1",
    "babel-loader": "^7.0.0",
    "babel-preset-react": "^6.24.1",
    "babel-preset-es2015": "^6.24.1",
    "react": "^15.5.4",
    "react-dom": "^15.5.4"
  },
  "name": "webpack-mvc5",
  "private": true
}
```

### webpack.config.js

```sh
var path = require("path");

module.exports = {
    entry: "./Content/main.jsx",
    output: {
        path: path.join(__dirname,"./Content/dist"),
        filename: "[name].js"
    },
    resolve: { extensions: ['.js', '.jsx'] },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loader: "babel-loader",
                exclude: /node_modules/,
                query: {
                    presets: ['react', 'es2015']
                }
            }
        ]
    }
};
```

### source/references

  * [http://www.sochix.ru/how-to-integrate-webpack-into-visual-studio-2015/](http://www.sochix.ru/how-to-integrate-webpack-into-visual-studio-2015/)
  * [https://www.twilio.com/blog/2015/08/setting-up-react-for-es6-with-webpack-and-babel-2.html](https://www.twilio.com/blog/2015/08/setting-up-react-for-es6-with-webpack-and-babel-2.html)

----

:earth_americas: [http://alansolisflores.com](http://alansolisflores.com)
