﻿var path = require("path");
var WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {
    entry: "./Content/main.jsx",
    output: {
        path: path.join(__dirname,"./Content/dist"),
        filename: "[name].js"
    },
    resolve: { extensions: ['.js', '.jsx'] },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loader: "babel-loader",
                exclude: /node_modules/,
                query: {
                    presets: ['react', 'es2015']
                }
            }
        ]
    },
    plugins: [
        new WebpackNotifierPlugin()
    ]
};