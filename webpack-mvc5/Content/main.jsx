﻿import React from 'react';
import ReactDOM from 'react-dom';

import CommentBox from './app/components/CommentBox';

ReactDOM.render(
    <CommentBox />,
    document.getElementById("content")
);